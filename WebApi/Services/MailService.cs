using System;
using MailKit.Security;
using MimeKit;
using MailKit.Net.Smtp;
namespace Services
{
    public class MailService
    {
        public static String usermail = "";
        public static String passwordmail = "";
        public static String smtpserver = "";
        public static String puerto = "";
        public static Boolean SendEmail(String receptor, String asunto, String mensaje)
        {
            SmtpClient smtp = new SmtpClient();
            var email = new MimeMessage();                                          //MENSAJE
            var builder = new BodyBuilder();                                       //BODY CONTRUCTOR
            var BodyHtmlView = "";                                                   //CUERPO DEL MENSAJE    
            try
            {
                if (smtpserver == "587")
                {
                    smtp.Connect("mail.ide-solution.pe", int.Parse(puerto), SecureSocketOptions.None);
                }
                else
                {
                    smtp.Connect("mail.ide-solution.pe", int.Parse(puerto), SecureSocketOptions.SslOnConnect);
                }

                smtp.Authenticate(usermail, passwordmail);
                email.From.Add(MailboxAddress.Parse(usermail));
                email.To.Add(MailboxAddress.Parse(receptor));

                email.Subject = asunto; // Sujeto del e-mail

                // Adicionando logo
                BodyHtmlView = string.Format(mensaje);
                builder.HtmlBody = BodyHtmlView;
                email.Body = builder.ToMessageBody();
                smtp.Send(email);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                smtp.Disconnect(true);
            }
        }
    }
}