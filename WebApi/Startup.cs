using System;
using System.IO;
using System.Text;
using CAD;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Services;
using Token;

namespace WebApi
{
    public class Startup
    {
        private readonly String _MyCors = "MyCors";
        public static String keyJWT;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddSingleton<TokenService>();
            Configuration = new ConfigurationBuilder()
                        // .AddJsonFile("appsettings.json")
                        .SetBasePath("/app")
                        // .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("appsettings")}.json")
                        // .AddJsonFile($"appsettings.developer.json")
                        .AddEnvironmentVariables()
                        .Build();
            MailService.usermail = Startup.Configuration.GetValue<String>("CredentialsCorreo:usuario");
            MailService.passwordmail = Startup.Configuration.GetValue<String>("CredentialsCorreo:contrasena");
            MailService.puerto = Startup.Configuration.GetValue<String>("CredentialsCorreo:puerto");
            MailService.smtpserver = Startup.Configuration.GetValue<String>("CredentialsCorreo:servidor");

            AD_Conector.ConexionBD = Startup.Configuration.GetValue<String>("ConnectionStrings:ConexionBDCovid");
            AD_Conector.UrlOneSignal = Startup.Configuration.GetValue<String>("ConnectionStrings:ConexionOneSignal");
            AD_Conector.AppId = Startup.Configuration.GetValue<String>("ConnectionStrings:AppId");
            AD_Conector.key = Startup.Configuration.GetValue<String>("ConnectionStrings:SecretKey");
            AD_Conector ad = new AD_Conector();
            ad.ObtenerDatos();
            var jwtSettigs = Configuration.GetSection("JwtSettings");
            string secretKey = jwtSettigs.GetValue<string>("SecretKey");
            int minutes = jwtSettigs.GetValue<int>("MinutesToExpiration");
            string issuer = jwtSettigs.GetValue<string>("Issuer");
            string audience = jwtSettigs.GetValue<string>("Audience");

            var key = Encoding.ASCII.GetBytes(secretKey);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(
                x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidIssuer = issuer,
                        ValidateAudience = true,
                        ValidAudience = audience,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(minutes)
                    };
                }
            );

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


        }
    }
}
