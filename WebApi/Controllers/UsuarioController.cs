using CEN;
using CLN;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
namespace WebApi
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class UsuarioController
    {
        //EDITAR NORMAL
        [HttpPost("editarUser/{userId}")]
        public UsuarioResponseEdit editar(UsuarioRequestEdit user, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.editar(user);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        //EDITAR ADMIN
        [HttpPost("editarUserAdm/{userId}")]
        public UsuarioResponseEdit editarAdm(UsuarioRequestEdit user, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.editarAdm(user);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        //INSERTAR USUARIO
        [HttpPost("insertUser/{userId}")]
        public UsuarioResponseInsert ingresar(UsuarioRequestInsert user, int userId)
        {
            Object idUsuarioEl = 0;
            try
            {
                //Primer Envío de Correo
                MailService.SendEmail(user.Correo, "Bienvenido al aplicativo Control Covid - IDE Solution",
                    "Le damos la bienvenida al aplicativo Control Covid - IDE Solution.");
                //Ingreso de usuario
                UsuarioCLN userCAD = new UsuarioCLN();
                UsuarioResponseInsert response = userCAD.ingresar(user);
                //Segundo Envío de Correo
                if (response.Codigo == 200)
                {
                    idUsuarioEl = response.Id;
                    MailService.SendEmail(user.Correo, "Envío de credencial del aplicativo Control Covid - IDE Solution",
                    response.Mensaje + " para acceder al sistema digitar su DNI y la siguiente contraseña: " + response.ClaveTemp + ". Posteriormente, cambiar la contraseña.");
                }
                return response;
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                if (ex.Message.Equals("No Such User Here\""))
                {
                    return new UsuarioResponseInsert()
                    {
                        Codigo = 400,
                        Mensaje = "El correo no existe"
                    };
                }
                else
                {
                    errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                    throw ex;
                }
            }
        }
        //ELIMINAR POR ID
        [HttpGet("eliminarUser/{id}/{userId}")]
        public UsuarioResponseEdit eliminar(int id, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.eliminar(id);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        //Listar trabajadores admind
        [HttpGet("listarUsers/{userId}")]
        public List<showUsuario> listarTrabajadores(int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.listarTrabajadores();
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        //Ver trabajador Por ID 
        [HttpGet("verUsuario/{id}/{userId}")]
        public showEditarUsuario verEdicion(int id, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.verEdicion(id);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        //DESHABILITAR
        [HttpGet("deshabilitar/{id}/{userId}")]
        public UsuarioResponseEdit deshabilitar(Int16 id, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.deshabilitar(id);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        //HABILITAR
        [HttpGet("habilitar/{id}/{userId}")]
        public UsuarioResponseEdit habilitar(Int16 id, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.habilitar(id);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
        [HttpPost("editarContra/{userId}")]
        public UsuarioResponseEdit cambiarContraFromUser(UsuarioRequestChangePassTemp user, int userId)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.cambiarContraFromUser(user);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", userId);
                throw ex;
            }
        }
    }
}