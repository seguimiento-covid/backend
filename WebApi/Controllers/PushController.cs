using System.Collections.Generic;
using CEN;
using CLN;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class PushController : Controller
    {
        [HttpPost]
        public string RetornarRespuesta(List<string> request)
        {

            PushCLN pushCLN = new PushCLN();
            PushRecibidoFrontCEN resp = pushCLN.traerCuerpo();
            resp.Personas = request;

            return pushCLN.enviarPush(resp);
        }
        [HttpGet("{id}/{appId}")]
        public bool AsignarId(int id, string appId)
        {
            PushCLN pushCLN = new PushCLN();
            return pushCLN.guardarAppId(id, appId);
        }
    }
}