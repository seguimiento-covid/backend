using CEN;
using CLN;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace WebApi.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class ReporteController : Controller
    {
        //Datos de Síntomas
        [HttpGet("sintomas/{userId}")]
        public List<Sintomas> listar(Int32 userId)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.listarSintomas();
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "Sintomas", userId);
                throw ex;
            }
        }
        //Datos de Signos de Riesgo
        [HttpGet("signos/{userId}")]
        public List<Signos> listarSignos(Int32 userId)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.listarSignos();
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "Signos", userId);
                throw ex;
            }
        }
        //Datos Para Vista Periodo
        [HttpGet("periodo/{mes}/{anio}/{userId}")]
        public List<reportePeriodoVista> listarPorPeriodo(int mes, int anio, int userId)
        {
            try
            {
                MesAnio mesa = new MesAnio();
                mesa.Mes = mes;
                mesa.Anio = anio;
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.reportePorPeriodo(mesa);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID-REPORTE", userId);
                throw ex;
            }
        }
        //Datos Para Vista Día Todos 
        [HttpGet("dia/{dia}/{mes}/{anio}/{userId}")]
        public List<reporteDiaVista> listarPorDia(int dia, int mes, int anio, int userId)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.reportePorPeriodo(dia, mes, anio);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID-REPORTE", userId);
                throw ex;
            }
        }
        //Datos Para Vista Personal por Día
        [HttpGet("personal/{dia}/{mes}/{anio}/{idUser}/{idJefe}")]
        public reporteDiaPersona listarPorDiaPersonal(int dia, int mes, int anio, int idUser, int idJefe)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.reportePersonalDia(dia, mes, anio, idUser);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID-REPORTE", idJefe);
                throw ex;
            }
        }
        //Datos Para Vista de Asignar Formulario
        [HttpGet("personas/covid/{userId}")]
        public List<FormCovidCEN> listarTieneCovid(int userId)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.reporteAsignarDia();
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID-REPORTE", userId);
                throw ex;
            }
        }
        [HttpGet("rango/{diaI}/{mesI}/{anioI}/{diaF}/{mesF}/{anioF}/{userId}")]
        public List<reporteRangoVista> reporteRangoVista(string diaI, string mesI, string anioI, string diaF, string mesF, string anioF, int userId)
        {
            EnvioRangoCEN envio = new EnvioRangoCEN();
            try
            {
                envio.FechaInicio = diaI + "-" + mesI + "-" + anioI;
                envio.FechaFinal = diaF + "-" + mesF + "-" + anioF;
                envio.UserId = userId;
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.listarRangos(envio);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID-REPORTE", userId);
                throw ex;
            }
        }
        [HttpGet("excel/{dia}/{mes}/{anio}/{userId}")]
        public List<ReporteExportExcel> obtenerExcel(int dia, int mes, int anio, int userId)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.listarExcel(dia, mes, anio);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID-REPORTE", userId);
                throw ex;
            }
        }


        //Guardar Reporte Normal
        [HttpPost]
        public bool guardarReporte(ReporteCEN reporte)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.agregarDetalle(reporte);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTE", reporte.UserId);
                throw ex;
            }
        }
        //Guardar Reporte Covid
        [HttpPost("covid")]
        public bool guardarReporteCovid(ReporteCovidCEN reporte)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.agregarDetalleCovid(reporte);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "REPORTECOVID", reporte.UserId);
                throw ex;
            }
        }

        //Cambiar Estado de Covid
        [HttpPost("covid/asignar")]
        public bool cambiarEstado(RecibeFormCovidCEN reporte)
        {
            try
            {
                ReporteCLN reporteCLN = new ReporteCLN();
                return reporteCLN.cambiarEstado(reporte);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "USUARIOIDE", reporte.UserMaster);
                throw ex;
            }
        }

    }
}