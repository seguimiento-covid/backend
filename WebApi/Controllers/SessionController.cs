using System;
using System.Collections.Generic;
using CEN;
using CLN;
using Microsoft.AspNetCore.Mvc;
using Token;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SessionController
    {
        private TokenService _tokenService;

        public SessionController(TokenService tokenService)
        {
            _tokenService = new TokenService(Startup.Configuration);
        }

        [HttpPost]
        public UsuarioResponseFront guardarReporte(UsuarioLog reporte)
        {
            UsuarioCLN reporteCLN = new UsuarioCLN();
            return cambiar(reporteCLN.login(reporte));
        }
        [HttpGet("tipoTra/{userId}")]
        public List<TipoTrabajadores> obtenerTiposTrabajadores(int userId)
        {
            try
            {
                UsuarioCLN reporteCLN = new UsuarioCLN();
                return reporteCLN.listarTipoTrabajadores();
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "Perfiles", userId);
                throw ex;
            }
        }

        private UsuarioResponseFront cambiar(UsuarioResponse rs)
        {
            UsuarioResponseFront respuesta = new UsuarioResponseFront();
            if (rs.Codigo == 200 || rs.Codigo == 205)
            {
                respuesta.Id = rs.Id;
                respuesta.Nombres = rs.Nombres;
                respuesta.Apellidos = rs.Apellidos;
                respuesta.Perfil = rs.Perfil;
                respuesta.Completado = rs.Completado;
                respuesta.Dni = rs.Dni;
                respuesta.Logueado = rs.Logueado;
                respuesta.HasCovid = rs.HasCovid;
                respuesta.Codigo = rs.Codigo;
                respuesta.Estado = rs.Estado;
                respuesta.Token = _tokenService.GenerarToken(rs);
            }
            return respuesta;
        }
        [HttpGet("version")]
        public string obtenerVersion()
        {
            UsuarioCLN reporteCLN = new UsuarioCLN();
            return reporteCLN.obtenerVersion();
        }
        [HttpGet("ExisteRecurso")]
        public string ExisteRecurso()
        {
            return "Existe Recurso App Covid Back v5.3.0";
        }


        [HttpPost("editarContra")]
        public UsuarioResponseEdit cambiarContraFromTemp(UsuarioRequestChangePassTemp user)
        {
            try
            {
                UsuarioCLN userCAD = new UsuarioCLN();
                return userCAD.cambiarContraFromTemp(user);
            }
            catch (Exception ex)
            {
                ErrorCLN errorCLN = new ErrorCLN();
                errorCLN.insertErrorLog(ex.Message, "UsuarioIde", user.Id);
                throw ex;
            }
        }








    }
}
