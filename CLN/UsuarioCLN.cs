using System;
using System.Collections.Generic;
using CAD;
using CEN;

namespace CLN
{
    public class UsuarioCLN
    {

        public UsuarioResponse login(UsuarioLog user)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.login(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TipoTrabajadores> listarTipoTrabajadores()
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.ObtenerTiposTrabajadores();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string obtenerVersion()
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.obtenerVersion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<showUsuario> listarTrabajadores()
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.listarTrabajadores();
            }
            catch (Exception ex) { throw ex; }
        }

        public UsuarioResponseEdit editar(UsuarioRequestEdit user)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.editar(user);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseEdit editarAdm(UsuarioRequestEdit user)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.editarAdm(user);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseInsert ingresar(UsuarioRequestInsert user)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.ingresar(user);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseEdit eliminar(int id)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.eliminar(id);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseEdit cambiarContraFromTemp(UsuarioRequestChangePassTemp user)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.cambiarContraFromTemp(user);
            }
            catch (Exception ex) { throw ex; }
        }
        public showEditarUsuario verEdicion(int id)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.verEdicion(id);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseEdit deshabilitar(Int16 id)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.deshabilitar(id);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseEdit habilitar(Int16 id)
        {
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.habilitar(id);
            }
            catch (Exception ex) { throw ex; }
        }
        public UsuarioResponseEdit cambiarContraFromUser(UsuarioRequestChangePassTemp user){
            try
            {
                UsuarioCAD userCAD = new UsuarioCAD();
                return userCAD.cambiarContraFromUser(user);
            }
            catch (Exception ex) { throw ex; }
        }

    }
}