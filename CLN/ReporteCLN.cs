﻿using System;
using System.Collections.Generic;
using CAD;
using CEN;

namespace CLN
{
    public class ReporteCLN
    {
        public bool agregarDetalle(ReporteCEN detalle)
        {
            try
            {
                ReporteCAD detalleOrdenCAD = new ReporteCAD();
                ReporteCEN response = detalleOrdenCAD.agregarReporte(detalle);
                foreach (var item in response.Sintomas)
                {
                    detalleOrdenCAD.agregarSintomas(item, response.Id);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool agregarDetalleCovid(ReporteCovidCEN detalle)
        {
            try
            {
                ReporteCAD detalleOrdenCAD = new ReporteCAD();
                ReporteCovidCEN response = detalleOrdenCAD.agregarReporteCovid(detalle);
                foreach (var item in response.Sintomas)
                {
                    detalleOrdenCAD.agregarSintomasCovid(item, response.Id);
                }
                foreach (var items in response.Signos)
                {
                    detalleOrdenCAD.agregarSignosCovid(items, response.Id);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Sintomas> listarSintomas()
        {
            try
            {
                SintomaCAD detalleOrdenCAD = new SintomaCAD();
                return detalleOrdenCAD.listarSintomas();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Signos> listarSignos()
        {
            try
            {
                SintomaCAD detalleOrdenCAD = new SintomaCAD();
                return detalleOrdenCAD.listarSignos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<reportePeriodoVista> reportePorPeriodo(MesAnio mesa)
        {
            try
            {
                VistasCAD rep = new VistasCAD();
                return rep.consulReportePeriodo(mesa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<reporteDiaVista> reportePorPeriodo(Int32 dia, Int32 mes, Int32 anio)
        {
            try
            {
                VistasCAD rep = new VistasCAD();
                return rep.consultaReporteDia(dia, mes, anio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public reporteDiaPersona reportePersonalDia(Int32 dia, Int32 mes, Int32 anio, Int32 idUser)
        {
            try
            {
                VistasCAD rep = new VistasCAD();
                return rep.obtenerReporteXDiaXPersona(dia, mes, anio, idUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FormCovidCEN> reporteAsignarDia()
        {
            try
            {
                VistasCAD userC = new VistasCAD();
                return userC.ObtenerUserCovidForm();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean cambiarEstado(RecibeFormCovidCEN recibe)
        {
            try
            {
                VistasCAD userC = new VistasCAD();
                return userC.cambiarEstado(recibe);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<reporteRangoVista> listarRangos(EnvioRangoCEN envioRango)
        {
            try
            {
                VistasCAD userC = new VistasCAD();
                return userC.consultarRangoFechas(envioRango);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ReporteExportExcel> listarExcel(Int32 dia, Int32 mes, Int32 anio)
        {
            try
            {
                VistasCAD rep = new VistasCAD();
                return rep.obtenerExcel(dia, mes, anio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
