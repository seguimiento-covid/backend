using System;
using CAD;
using CEN;

namespace CLN
{
    public class ErrorCLN
    {
        public bool insertErrorLog(String Mensaje, String TablaRef, Int32 Codigo)
        {
            try
            {
                ErrorCEN error = new ErrorCEN();
                error.Operacion = Mensaje;
                error.TablaRef = TablaRef;
                error.UsuarioId = Codigo;

                ErrorCAD errorCAD = new ErrorCAD();
                errorCAD.agregarError(error);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}