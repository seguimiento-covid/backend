using System;
using CAD;
using CEN;

namespace CLN
{
    public class PushCLN
    {
        public String enviarPush(PushRecibidoFrontCEN request)
        {
            try
            {
                PushCAD pushCAD = new PushCAD();
                var vari = pushCAD.sendPushs(request);
                return vari.Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool guardarAppId(int id, string appId)
        {
            try
            {
                Console.WriteLine(id);
                Console.WriteLine(appId);
                PushCAD pushCAD = new PushCAD();
                return pushCAD.guardarAppId(id, appId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public PushRecibidoFrontCEN traerCuerpo()
        {
            try
            {
                PushCAD pushCAD = new PushCAD();
                return pushCAD.traerCuerpo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}