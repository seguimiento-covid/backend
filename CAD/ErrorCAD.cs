using System;
using System.Data;
using System.Data.SqlClient;
using CEN;

namespace CAD
{
    public class ErrorCAD
    {
        private SqlConnection connection;
        private readonly AD_Conector _datosConexionGuia;
        private readonly SqlCommand _command;
        public ErrorCAD()
        {
            _command = new SqlCommand();
            _datosConexionGuia = new AD_Conector();
            connection = new SqlConnection(_datosConexionGuia.CxSQLFacturacion());
        }
        public bool agregarError(ErrorCEN reporte)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_insertarError", connection);
                cmd.Parameters.AddWithValue("@operacion", reporte.Operacion);
                cmd.Parameters.AddWithValue("@tblRef", reporte.TablaRef);
                cmd.Parameters.AddWithValue("@userId", reporte.UsuarioId);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}