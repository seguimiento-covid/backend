using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;
using Microsoft.VisualBasic;

namespace CAD
{
    public class VistasCAD
    {
        private SqlConnection connection;
        private readonly AD_Conector _datosConexionGuia;
        private readonly SqlCommand _command;
        public VistasCAD()
        {
            _command = new SqlCommand();
            _datosConexionGuia = new AD_Conector();
            connection = new SqlConnection(_datosConexionGuia.CxSQLFacturacion());
        }
        public List<reportePeriodoVista> consulReportePeriodo(MesAnio mesa)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            reportePeriodoVista response = new reportePeriodoVista();
            String recibeDias = "";
            List<reportePeriodoVista> cursorMaestro = new List<reportePeriodoVista>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarReporteXPeriodo", connection);
                cmd.Parameters.AddWithValue("@mes", mesa.Mes);
                cmd.Parameters.AddWithValue("@anio", mesa.Anio);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {

                        response = new reportePeriodoVista();
                        recibeDias = "";
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["Dni"]), "", Rs["Dni"]);
                        response.Nombre = (String)Interaction.IIf(Information.IsDBNull(Rs["Nombre"]), "", Rs["Nombre"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["TipoTrabajador"]), "", Rs["TipoTrabajador"]);
                        recibeDias = (String)Interaction.IIf(Information.IsDBNull(Rs["Dias"]), "", Rs["Dias"]);
                        if (!recibeDias.Equals(""))
                        {
                            int[] ints = Array.ConvertAll(recibeDias.Split('/', StringSplitOptions.RemoveEmptyEntries), s => int.Parse(s));
                            response.Dias = ints;
                        }
                        else
                        {
                            response.Dias = new Int32[0];
                        }
                        cursorMaestro.Add(response);
                    }
                }
                return cursorMaestro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public List<reporteDiaVista> consultaReporteDia(Int32 dia, Int32 mes, Int32 anio)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            reporteDiaVista response = new reporteDiaVista();
            List<reporteDiaVista> cursorMaestro = new List<reporteDiaVista>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarReporteXDia", connection);
                cmd.Parameters.AddWithValue("@dia", dia);
                cmd.Parameters.AddWithValue("@mes", mes);
                cmd.Parameters.AddWithValue("@anio", anio);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int16 valor = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new reporteDiaVista();
                        response.Id = (Int16)Interaction.IIf(Information.IsDBNull(Rs["Id"]), valor, Rs["Id"]);
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["Dni"]), "", Rs["Dni"]);
                        response.Nombre = (String)Interaction.IIf(Information.IsDBNull(Rs["Nombre"]), "", Rs["Nombre"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["Perfils"]), "", Rs["Perfils"]);
                        response.Estado = (Int16)Interaction.IIf(Information.IsDBNull(Rs["Status"]), 0, Rs["Status"]);
                        response.Descripcion = (String)Interaction.IIf(Information.IsDBNull(Rs["Descripcion"]), "", Rs["Descripcion"]);
                        response.AppId = (String)Interaction.IIf(Information.IsDBNull(Rs["AppId"]), "", Rs["AppId"]);
                        cursorMaestro.Add(response);
                    }
                }
                return cursorMaestro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public reporteDiaPersona obtenerReporteXDiaXPersona(Int32 dia, Int32 mes, Int32 anio, Int32 idUser)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            reporteDiaPersona response = new reporteDiaPersona();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarPersonaDiaReporte", connection);
                cmd.Parameters.AddWithValue("@userId", idUser);
                cmd.Parameters.AddWithValue("@dia", dia);
                cmd.Parameters.AddWithValue("@mes", mes);
                cmd.Parameters.AddWithValue("@anio", anio);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int16 valor = 0;
                Decimal val = 0;
                Int32 valor32 = 32;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Id = (Int16)Interaction.IIf(Information.IsDBNull(Rs["IdUser"]), valor, Rs["IdUser"]);
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["Dni"]), "", Rs["Dni"]);
                        response.Nombre = (String)Interaction.IIf(Information.IsDBNull(Rs["Nombres"]), "", Rs["Nombres"]);
                        response.Apellido = (String)Interaction.IIf(Information.IsDBNull(Rs["Apellidos"]), "", Rs["Apellidos"]);
                        response.Telefono = (String)Interaction.IIf(Information.IsDBNull(Rs["Telefono"]), "", Rs["Telefono"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["Perfils"]), "", Rs["Perfils"]);
                        response.IdReporte = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Id"]), valor32, Rs["Id"]);
                        response.FechaReporte = (DateTime)Interaction.IIf(Information.IsDBNull(Rs["FechaRep"]), valor32, Rs["FechaRep"]);
                        response.Sintomas = devolverCosas((String)Interaction.IIf(Information.IsDBNull(Rs["Sintomas"]), "", Rs["Sintomas"]));
                        response.Correo = (String)Interaction.IIf(Information.IsDBNull(Rs["Correo"]), "", Rs["Correo"]);
                        response.Direccion = (String)Interaction.IIf(Information.IsDBNull(Rs["Direccion"]), "", Rs["Direccion"]);
                        if ((Boolean)Rs["Normal"])
                        {
                            response.CantDosis = (Int16)Interaction.IIf(Information.IsDBNull(Rs["CantDosis"]), valor, Rs["CantDosis"]);
                            response.Confirmado = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["Confirmado"]), false, Rs["Confirmado"]);
                            response.Sospechozo = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["Sospechozo"]), false, Rs["Sospechozo"]);
                            response.Positivo = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["Positivo"]), false, Rs["Positivo"]);
                        }
                        else
                        {
                            response.Temperatura = (Decimal)Interaction.IIf(Information.IsDBNull(Rs["Temp"]), val, Rs["Temp"]);
                            response.Comentario = (String)Interaction.IIf(Information.IsDBNull(Rs["Comentario"]), "", Rs["Comentario"]);
                            response.Signos = devolverCosas((String)Interaction.IIf(Information.IsDBNull(Rs["Signos"]), "", Rs["Signos"]));
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public String[] devolverCosas(String patron)
        {
            if (!patron.Equals(""))
            {
                String[] ints = patron.Split('|', StringSplitOptions.RemoveEmptyEntries);
                return ints;
            }
            else
            {
                return new String[0];
            }
        }
        public List<FormCovidCEN> ObtenerUserCovidForm()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            FormCovidCEN response = new FormCovidCEN();
            List<FormCovidCEN> cursorMaestro = new List<FormCovidCEN>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarUsuarioCovid", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int16 valor = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new FormCovidCEN();
                        response.Id = (Int16)Interaction.IIf(Information.IsDBNull(Rs["Id"]), valor, Rs["Id"]);
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["Dni"]), "", Rs["Dni"]);
                        response.Nombre = (String)Interaction.IIf(Information.IsDBNull(Rs["Nombre"]), "", Rs["Nombre"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["Perfils"]), "", Rs["Perfils"]);
                        response.HasCovid = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["HasCovid"]), false, Rs["HasCovid"]);
                        cursorMaestro.Add(response);
                    }
                }
                return cursorMaestro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public Boolean cambiarEstado(RecibeFormCovidCEN recibe)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            String codigo = "";
            String mensaje = "";
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_changeEstatusCovid", connection);
                cmd.Parameters.AddWithValue("@userId", recibe.Id);
                cmd.Parameters.AddWithValue("@hascovid", recibe.HasCovid);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        codigo = (String)Interaction.IIf(Information.IsDBNull(Rs["CODIGO"]), "", Rs["CODIGO"]);
                        mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["MENSAJE"]), "", Rs["MENSAJE"]);
                    }
                }
                if (codigo.Equals("200"))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public List<reporteRangoVista> consultarRangoFechas(EnvioRangoCEN envioRango)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            reporteRangoVista response = new reporteRangoVista();
            String recibeDias = "";
            List<reporteRangoVista> cursorMaestro = new List<reporteRangoVista>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarRangos", connection);
                cmd.Parameters.AddWithValue("@FechaI", envioRango.FechaInicio);
                cmd.Parameters.AddWithValue("@FechaF", envioRango.FechaFinal);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new reporteRangoVista();
                        recibeDias = "";
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["Dni"]), "", Rs["Dni"]);
                        response.Nombre = (String)Interaction.IIf(Information.IsDBNull(Rs["Nombre"]), "", Rs["Nombre"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["TipoTrabajador"]), "", Rs["TipoTrabajador"]);
                        recibeDias = (String)Interaction.IIf(Information.IsDBNull(Rs["Dias"]), "", Rs["Dias"]);
                        if (!recibeDias.Equals(""))
                        {
                            int[] ints = Array.ConvertAll(recibeDias.Split('|', StringSplitOptions.RemoveEmptyEntries), s => int.Parse(s));
                            response.Dias = ints;
                        }
                        else
                        {
                            response.Dias = new Int32[0];
                        }
                        cursorMaestro.Add(response);
                    }
                }
                return cursorMaestro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<ReporteExportExcel> obtenerExcel(Int32 dia, Int32 mes, Int32 anio)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            ReporteExportExcel response = new ReporteExportExcel();
            List<ReporteExportExcel> cursor = new List<ReporteExportExcel>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarParaDiaExcel", connection);
                cmd.Parameters.AddWithValue("@dia", dia);
                cmd.Parameters.AddWithValue("@mes", mes);
                cmd.Parameters.AddWithValue("@anio", anio);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Decimal val = 0;
                Int32 valor32 = 32;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new ReporteExportExcel();
                        response.Id = (Int32)Interaction.IIf(Information.IsDBNull(Rs["IdUser"]), valor32, Rs["IdUser"]);
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["Dni"]), "", Rs["Dni"]);
                        response.Nombre = (String)Interaction.IIf(Information.IsDBNull(Rs["Nombres"]), "", Rs["Nombres"]);
                        response.Apellido = (String)Interaction.IIf(Information.IsDBNull(Rs["Apellidos"]), "", Rs["Apellidos"]);
                        response.Telefono = (String)Interaction.IIf(Information.IsDBNull(Rs["Telefono"]), "", Rs["Telefono"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["Cargo"]), "", Rs["Cargo"]);
                        response.IdReporte = (Int32)Interaction.IIf(Information.IsDBNull(Rs["IdRep"]), valor32, Rs["IdRep"]);
                        response.FechaReporte = (DateTime)Interaction.IIf(Information.IsDBNull(Rs["FechaRep"]), valor32, Rs["FechaRep"]);
                        response.Sintomas = devolverCosas((String)Interaction.IIf(Information.IsDBNull(Rs["Sintomas"]), "", Rs["Sintomas"]));
                        response.Correo = (String)Interaction.IIf(Information.IsDBNull(Rs["Correo"]), "", Rs["Correo"]);
                        response.Direccion = (String)Interaction.IIf(Information.IsDBNull(Rs["Direccion"]), "", Rs["Direccion"]);
                        response.Normal = (Byte)Rs["Normales"] == 1 ? true : false;
                        if (response.Normal)
                        {
                            response.CantDosis = (Int16)Interaction.IIf(Information.IsDBNull(Rs["CantDosis"]), 0, Rs["CantDosis"]);
                            response.Confirmado = (Byte)Rs["Confirmado"] == 1 ? true : false;
                            response.Sospechozo = (Byte)Rs["Sospechozo"] == 1 ? true : false;
                            response.Positivo = (Byte)Rs["Positivo"] == 1 ? true : false;
                        }
                        else
                        {
                            response.Temperatura = (Decimal)Interaction.IIf(Information.IsDBNull(Rs["Temp"]), val, Rs["Temp"]);
                            response.Comentario = (String)Interaction.IIf(Information.IsDBNull(Rs["Comentario"]), "", Rs["Comentario"]);
                            response.Signos = devolverCosas((String)Interaction.IIf(Information.IsDBNull(Rs["Signos"]), "", Rs["Signos"]));
                        }
                        cursor.Add(response);
                    }
                }
                return cursor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

    }
}