using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;
using Microsoft.VisualBasic;

namespace CAD
{
    public class SintomaCAD
    {
        private SqlConnection connection;
        private readonly AD_Conector _datosConexionGuia;
        private readonly SqlCommand _command;
        public SintomaCAD()
        {
            _command = new SqlCommand();
            _datosConexionGuia = new AD_Conector();
            connection = new SqlConnection(_datosConexionGuia.CxSQLFacturacion());
        }
        public List<Signos> listarSignos()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            List<Signos> cursor = new List<Signos>();
            Signos response = new Signos();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarSignos", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int16 valor64 = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new Signos();
                        response.SignoId = (Int16)Interaction.IIf(Information.IsDBNull(Rs["Sig_Id"]), valor64, Rs["Sig_Id"]);
                        response.Detalle = (String)Interaction.IIf(Information.IsDBNull(Rs["Sig_Detalle"]), "", Rs["Sig_Detalle"]);
                        cursor.Add(response);
                    }
                }
                return cursor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public List<Sintomas> listarSintomas()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            List<Sintomas> cursor = new List<Sintomas>();
            Sintomas response = new Sintomas();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarSintomas", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int32 valor64 = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new Sintomas();
                        response.SintomaId = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Sint_Id"]), valor64, Rs["Sint_Id"]);
                        response.Detalle = (String)Interaction.IIf(Information.IsDBNull(Rs["Sint_Detalle"]), "", Rs["Sint_Detalle"]);
                        cursor.Add(response);
                    }
                }
                return cursor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}