using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;
using Microsoft.VisualBasic;

namespace CAD
{
    public class UsuarioCAD
    {

        private SqlConnection connection;
        private readonly AD_Conector _datosConexionGuia;
        private readonly SqlCommand _command;
        public UsuarioCAD()
        {
            _command = new SqlCommand();
            _datosConexionGuia = new AD_Conector();
            connection = new SqlConnection(_datosConexionGuia.CxSQLFacturacion());
        }
        public UsuarioResponse login(UsuarioLog usuario)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponse response = new UsuarioResponse();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_LoginAndValidCompleted", connection);
                cmd.Parameters.AddWithValue("@dni", usuario.Dni);
                cmd.Parameters.AddWithValue("@pass", usuario.Password);
                Int16 valor32 = 0;
                int valor64 = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Logueado = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["Logueado"]), false, Rs["Logueado"]);
                        response.Codigo = (int)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), valor64, Rs["Codigo"]);
                        if (response.Logueado)
                        {
                            response.HasCovid = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["HasCovid"]), false, Rs["HasCovid"]);
                            response.Completado = (Boolean)Interaction.IIf(Information.IsDBNull(Rs["Completado"]), false, Rs["Completado"]);
                            response.Id = (Int16)Interaction.IIf(Information.IsDBNull(Rs["User_Id"]), valor32, Rs["User_Id"]);
                            response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Dni"]), "", Rs["User_Dni"]);
                            response.Nombres = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Nombres"]), "", Rs["User_Nombres"]);
                            response.Apellidos = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Apellidos"]), "", Rs["User_Apellidos"]);
                            response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["Perfils"]), "", Rs["Perfils"]);
                            response.Estado = (Int16)Interaction.IIf(Information.IsDBNull(Rs["Estado"]), valor32, Rs["Estado"]);
                        }
                        else
                        {
                            response.Id = valor32;
                            response.Dni = "";
                            response.Nombres = "";
                            response.Apellidos = "";
                            response.Completado = false;
                            response.HasCovid = false;
                            response.Perfil = "";
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public List<TipoTrabajadores> ObtenerTiposTrabajadores()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            TipoTrabajadores response = new TipoTrabajadores();
            List<TipoTrabajadores> cursorMaestro = new List<TipoTrabajadores>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarTiposTrabajador", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Byte valor = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new TipoTrabajadores();
                        response.Id = (Byte)Interaction.IIf(Information.IsDBNull(Rs["Per_Id"]), valor, Rs["Per_Id"]);
                        response.Detalle = (String)Interaction.IIf(Information.IsDBNull(Rs["Per_Detalle"]), "", Rs["Per_Detalle"]);
                        cursorMaestro.Add(response);
                    }
                }
                return cursorMaestro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public string obtenerVersion()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            string retorn = "";
            try
            {
                connection.Open();
                cmd = new SqlCommand("retornarLastVersion", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        retorn = (String)Interaction.IIf(Information.IsDBNull(Rs["version"]), "", Rs["version"]);
                    }
                }
                return retorn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        //CRUD Géstión Usuario
        public List<showUsuario> listarTrabajadores()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            showUsuario response = new showUsuario();
            List<showUsuario> block = new List<showUsuario>();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_listarUsuarios", connection);
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response = new showUsuario();
                        response.Id = (Int16)Interaction.IIf(Information.IsDBNull(Rs["User_Id"]), "", Rs["User_Id"]);
                        response.Nombres = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Nombres"]), "", Rs["User_Nombres"]);
                        response.Apellidos = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Apellidos"]), "", Rs["User_Apellidos"]);
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Dni"]), "", Rs["User_Dni"]);
                        response.Telefono = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Telefono"]), "", Rs["User_Telefono"]);
                        response.Correo = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Correo"]), "", Rs["User_Correo"]);
                        response.Direccion = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Direccion"]), "", Rs["User_Direccion"]);
                        response.Perfil = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Per_Id"]), 0, Rs["User_Per_Id"]);
                        response.Estado = (String)Interaction.IIf(Information.IsDBNull(Rs["Estado"]), 0, Rs["Estado"]);
                        block.Add(response);
                    }
                }
                return block;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit editar(UsuarioRequestEdit user)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                Console.WriteLine(user.Nombres);
                connection.Open();
                cmd = new SqlCommand("sp_editarUsuario", connection);
                cmd.Parameters.AddWithValue("@id", user.Id);
                cmd.Parameters.AddWithValue("@nombres", user.Nombres);
                cmd.Parameters.AddWithValue("@apellidos", user.Apellidos);
                cmd.Parameters.AddWithValue("@correo", user.Correo);
                cmd.Parameters.AddWithValue("@telefono", user.Telefono);
                cmd.Parameters.AddWithValue("@direccion", user.Direccion);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                while (Rs.Read())
                {
                    if (Rs.HasRows)
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit editarAdm(UsuarioRequestEdit user)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_editarUsuarioAdmin", connection);
                cmd.Parameters.AddWithValue("@id", user.Id);
                cmd.Parameters.AddWithValue("@dni", user.Dni);
                cmd.Parameters.AddWithValue("@nombres", user.Nombres);
                cmd.Parameters.AddWithValue("@apellidos", user.Apellidos);
                cmd.Parameters.AddWithValue("@correo", user.Correo);
                cmd.Parameters.AddWithValue("@telefono", user.Telefono);
                cmd.Parameters.AddWithValue("@direccion", user.Direccion);
                cmd.Parameters.AddWithValue("@perfil",user.PerfilId);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                while (Rs.Read())
                {
                    if (Rs.HasRows)
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseInsert ingresar(UsuarioRequestInsert user)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseInsert response = new UsuarioResponseInsert();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_insertUsuario", connection);
                cmd.Parameters.AddWithValue("@dni", user.Dni);
                cmd.Parameters.AddWithValue("@nombre", user.Nombres);
                cmd.Parameters.AddWithValue("@apellido", user.Apellidos);
                cmd.Parameters.AddWithValue("@correo", user.Correo);
                cmd.Parameters.AddWithValue("@telefono", user.Telefono);
                cmd.Parameters.AddWithValue("@direccion", user.Direccion);
                cmd.Parameters.AddWithValue("@perfilId", user.PerfilId);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Id = (Object)Interaction.IIf(Information.IsDBNull(Rs["User_Id"]), 0, Rs["User_Id"]);
                        response.ClaveTemp = (String)Interaction.IIf(Information.IsDBNull(Rs["PasswordTemp"]), "", Rs["PasswordTemp"]);
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit eliminar(int id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_eliminarUsuario", connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public showEditarUsuario verEdicion(int id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            showEditarUsuario response = new showEditarUsuario();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_verUsuarioXId", connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                while (Rs.Read())
                {
                    if (Rs.HasRows)
                    {
                        response.Id = (Int16)Interaction.IIf(Information.IsDBNull(Rs["User_Id"]), "", Rs["User_Id"]);
                        response.Nombres = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Nombres"]), "", Rs["User_Nombres"]);
                        response.Apellidos = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Apellidos"]), "", Rs["User_Apellidos"]);
                        response.Dni = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Dni"]), "", Rs["User_Dni"]);
                        response.Telefono = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Telefono"]), "", Rs["User_Telefono"]);
                        response.Correo = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Correo"]), "", Rs["User_Correo"]);
                        response.Direccion = (String)Interaction.IIf(Information.IsDBNull(Rs["User_Direccion"]), "", Rs["User_Direccion"]);
                        response.PerfilId = (Byte)Interaction.IIf(Information.IsDBNull(Rs["User_Per_Id"]), 0, Rs["User_Per_Id"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit cambiarContraFromTemp(UsuarioRequestChangePassTemp user)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_changePassFromTemp", connection);
                cmd.Parameters.AddWithValue("@id", user.Id);
                cmd.Parameters.AddWithValue("@passTemp", user.ClaveAnterior);
                cmd.Parameters.AddWithValue("@passNew", user.ClaveNueva);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit deshabilitar(Int16 id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_deshabilitarUser", connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit habilitar(Int16 id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_habilitarUser", connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public UsuarioResponseEdit cambiarContraFromUser(UsuarioRequestChangePassTemp user){
            SqlCommand cmd;
            SqlDataReader Rs;
            UsuarioResponseEdit response = new UsuarioResponseEdit();
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_editarClaveUsuario", connection);
                cmd.Parameters.AddWithValue("@id", user.Id);
                cmd.Parameters.AddWithValue("@claveAnterior", user.ClaveAnterior);
                cmd.Parameters.AddWithValue("@claveNueva", user.ClaveNueva);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Codigo = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Codigo"]), 0, Rs["Codigo"]);
                        response.Mensaje = (String)Interaction.IIf(Information.IsDBNull(Rs["Mensaje"]), "", Rs["Mensaje"]);
                    }
                }
                return response;
            } catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
