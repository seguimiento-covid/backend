﻿using System.Net.Http;

namespace CAD
{
    public class AD_Conector
    {
        private HttpClient httpClient;
        public static string ConexionBD { get; set; }
        public static string UrlOneSignal { get; set; }
        public static string AppId { get; set; }
        public static string key { get; set; }

        public async void ObtenerDatos()
        {
            httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.GetAsync(ConexionBD);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            AD_Conector.ConexionBD = responseBody;
            // AD_Conector.ConexionBD = "Server=servidorsqlidepartner.database.windows.net,1433;Database=bd_prueba;Uid=cvallejos;Pwd=CVallej0s@2021;";
        }

        public string CxSQLFacturacion()
        {
            return ConexionBD;
        }
        public string CxApiOneConfig()
        {
            return UrlOneSignal;
        }
        public string keyApiOneConfig()
        {
            return key;

        }
        public string AppApiOneConfig()
        {
            return AppId;
        }
    }
}
