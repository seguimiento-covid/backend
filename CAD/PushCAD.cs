using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CEN;
using Microsoft.VisualBasic;
using Newtonsoft.Json;

namespace CAD
{
    public class PushCAD
    {
        private SqlConnection connection;
        private readonly AD_Conector _datosConexionGuia;
        private HttpClient client;
        public PushCAD()
        {
            _datosConexionGuia = new AD_Conector();
            connection = new SqlConnection(_datosConexionGuia.CxSQLFacturacion());
        }
        public async Task<String> sendPushs(PushRecibidoFrontCEN request)
        {
            client = new HttpClient();
            PushSendCEN pushSendCEN = new PushSendCEN();
            pushSendCEN.contents = new Content();
            pushSendCEN.headings = new Headings();
            try
            {
                String Url = _datosConexionGuia.CxApiOneConfig();
                pushSendCEN.app_id = _datosConexionGuia.AppApiOneConfig();
                pushSendCEN.contents.en = request.Texto;
                pushSendCEN.headings.en = request.Cabecera;
                pushSendCEN.include_player_ids = request.Personas;
                var key = new AuthenticationHeaderValue("Basic", _datosConexionGuia.keyApiOneConfig());
                client.DefaultRequestHeaders.Authorization = key;
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                var contenido = JsonConvert.SerializeObject(pushSendCEN);
                var content = new StringContent(contenido.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(Url, content);
                String responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool guardarAppId(int id, string appId)
        {
            SqlCommand cmd;
            try
            {
                connection.Open();
                cmd = new SqlCommand("asignarIdApp", connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@appId", appId);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteReader();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public PushRecibidoFrontCEN traerCuerpo()
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            PushRecibidoFrontCEN response = new PushRecibidoFrontCEN();
            try
            {
                connection.Open();
                cmd = new SqlCommand("traerCuerpoPush", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Cabecera = (String)Interaction.IIf(Information.IsDBNull(Rs["cabecera"]), "", Rs["cabecera"]);
                        response.Texto = (String)Interaction.IIf(Information.IsDBNull(Rs["contenido"]), "", Rs["contenido"]);

                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}