﻿using CEN;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;

namespace CAD
{
    public class ReporteCAD
    {
        private SqlConnection connection;
        private readonly AD_Conector _datosConexionGuia;
        private readonly SqlCommand _command;
        public ReporteCAD()
        {
            _command = new SqlCommand();
            _datosConexionGuia = new AD_Conector();
            connection = new SqlConnection(_datosConexionGuia.CxSQLFacturacion());
        }
        public ReporteCEN agregarReporte(ReporteCEN reporte)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            ReporteCEN response = reporte;
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_IngresarReporte", connection);
                cmd.Parameters.AddWithValue("@sospechozo", reporte.ContactoSospechozo);
                cmd.Parameters.AddWithValue("@confirmado", reporte.ContactoConfirmado);
                cmd.Parameters.AddWithValue("@positivo", reporte.Positivo);
                cmd.Parameters.AddWithValue("@cantDosis", reporte.CantDosis);
                cmd.Parameters.AddWithValue("@fechaPer", reporte.Fecha.AddHours(-5));
                cmd.Parameters.AddWithValue("@userId", reporte.UserId);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int32 valor64 = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Id = (Int32)Interaction.IIf(Information.IsDBNull(Rs["Rep_Id"]), valor64, Rs["Rep_Id"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public bool agregarSintomas(SintomaRecive sintomas, Int32 id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            String cod = "";
            String message = "";
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_IngresarSubReportes", connection);
                cmd.Parameters.AddWithValue("@Reporte_ID", id);
                cmd.Parameters.AddWithValue("@Sintoma_ID", sintomas.SintomaId);
                cmd.Parameters.AddWithValue("@OtroDetalle", sintomas.OtroDetalle);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        cod = (String)Interaction.IIf(Information.IsDBNull(Rs["CODIGO"]), "", Rs["CODIGO"]);
                        message = (String)Interaction.IIf(Information.IsDBNull(Rs["MENSAJE"]), "", Rs["MENSAJE"]);
                        if (cod.Equals("400"))
                        {
                            Console.WriteLine("Error");
                        }
                    }
                }
                if (cod.Equals("200"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }

        }
        public ReporteCovidCEN agregarReporteCovid(ReporteCovidCEN reporte)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            ReporteCovidCEN response = reporte;
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_IngresarReporteCovid", connection);
                cmd.Parameters.AddWithValue("@userId", reporte.UserId);
                cmd.Parameters.AddWithValue("@temp", reporte.Temperatura);
                cmd.Parameters.AddWithValue("@fechaPer", reporte.Fecha.AddHours(-5));
                cmd.Parameters.AddWithValue("@comentario", reporte.Comentario);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                Int32 valor64 = 0;
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        response.Id = (Int32)Interaction.IIf(Information.IsDBNull(Rs["REPC_Id"]), valor64, Rs["REPC_Id"]);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public bool agregarSintomasCovid(SintomaRecive sintomas, Int32 id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            String cod = "";
            String message = "";
            try
            {
                Console.WriteLine(id);
                connection.Open();
                cmd = new SqlCommand("sp_IngresarSubReportesSintomasCovid", connection);
                cmd.Parameters.AddWithValue("@Reporte_ID", id);
                cmd.Parameters.AddWithValue("@Sintoma_ID", sintomas.SintomaId);
                cmd.Parameters.AddWithValue("@OtroDetalle", sintomas.OtroDetalle);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        cod = (String)Interaction.IIf(Information.IsDBNull(Rs["CODIGO"]), "", Rs["CODIGO"]);
                        message = (String)Interaction.IIf(Information.IsDBNull(Rs["MENSAJE"]), "", Rs["MENSAJE"]);
                        if (cod.Equals("400"))
                        {
                            Console.WriteLine("Error");
                        }
                    }
                }
                if (cod.Equals("200"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public bool agregarSignosCovid(SignoRecive sintomas, Int32 id)
        {
            SqlCommand cmd;
            SqlDataReader Rs;
            String cod = "";
            String message = "";
            try
            {
                connection.Open();
                cmd = new SqlCommand("sp_ingresarSubReporteSignos", connection);
                cmd.Parameters.AddWithValue("@reporteId", id);
                cmd.Parameters.AddWithValue("@SigId", sintomas.SignoId);
                cmd.Parameters.AddWithValue("@Otro", sintomas.OtroDetalle);
                cmd.CommandType = CommandType.StoredProcedure;
                Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        cod = (String)Interaction.IIf(Information.IsDBNull(Rs["CODIGO"]), "", Rs["CODIGO"]);
                        message = (String)Interaction.IIf(Information.IsDBNull(Rs["MENSAJE"]), "", Rs["MENSAJE"]);
                        if (cod.Equals("400"))
                        {
                            Console.WriteLine("Error");
                        }
                    }
                }
                if (cod.Equals("200"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
