﻿using System;
using System.Collections.Generic;

namespace CEN
{
    public class ReporteCEN
    {
        public Int32 Id { get; set; }
        public DateTime Fecha { get; set; }
        public Int16 ContactoSospechozo { get; set; }
        public Int16 ContactoConfirmado { get; set; }
        public Int16 Positivo { get; set; }
        public Int16 CantDosis { get; set; }
        public Int32 UserId { get; set; }
        public List<SintomaRecive> Sintomas { get; set; }
    }

    public class ReporteCovidCEN
    {
        public Int32 Id { get; set; }
        public DateTime Fecha { get; set; }
        public Int32 UserId { get; set; }
        public Decimal Temperatura { get; set; }
        public String Comentario { get; set; }
        public List<SintomaRecive> Sintomas { get; set; }
        public List<SignoRecive> Signos { get; set; }
    }
}
