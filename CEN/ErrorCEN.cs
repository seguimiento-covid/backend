using System;

namespace CEN
{
    public class ErrorCEN
    {
        public String Operacion { get; set; }
        public String TablaRef { get; set; }
        public Int32 UsuarioId { get; set; }
    }
}