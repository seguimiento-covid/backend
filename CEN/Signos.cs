using System;

namespace CEN
{
    public class Signos
    {
        public Int16 SignoId { get; set; }
        public String Detalle { get; set; }
    }

    public class SignoRecive
    {
        public Int16 SignoId { get; set; }
        public String OtroDetalle { get; set; }
    }

    public class SignoPersonalReporte
    {
        public String Signo { get; set; }
        public String OtroDetalle { get; set; }
    }
}