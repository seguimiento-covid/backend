using System;

namespace CEN
{
    public class UsuarioResponse
    {
        public Int16 Id { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String Dni { get; set; }
        public Boolean Completado { get; set; }
        public Boolean HasCovid { get; set; }
        public Boolean Logueado { get; set; }
        public String Perfil { get; set; }
        public int Codigo { get; set; }
        public Int16 Estado { get; set; }
    }
    public class UsuarioLog
    {
        public String Dni { get; set; }
        public String Password { get; set; }
    }

    public class TipoTrabajadores
    {
        public Byte Id { get; set; }
        public String Detalle { get; set; }
    }

    public class UsuarioResponseFront
    {
        public Int16 Id { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String Dni { get; set; }
        public Boolean Completado { get; set; }
        public Boolean HasCovid { get; set; }
        public Boolean Logueado { get; set; }
        public String Perfil { get; set; }
        public int Codigo { get; set; }
        public String Token { get; set; }
        public Int16 Estado { get; set; }
    }
    public class UsuarioRequestEdit
    {
        public Int32 Id { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String Dni { get; set; }
        public String Correo { get; set; }
        public String Telefono { get; set; }
        public String Direccion { get; set; }
        public Int16 PerfilId { get; set; }
    }
    public class UsuarioResponseEdit
    {
        public String Mensaje { get; set; }
        public Int32 Codigo { get; set; }
    }
    public class UsuarioRequestInsert
    {
        public Int16 PerfilId { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String Dni { get; set; }
        public String Correo { get; set; }
        public String Telefono { get; set; }
        public String Direccion { get; set; }
    }
    public class UsuarioResponseInsert
    {
        public Object Id { get; set; }
        public String ClaveTemp { get; set; }
        public String Mensaje { get; set; }
        public Int32 Codigo { get; set; }
    }
    public class showUsuario
    {
        public Int16 Id { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String Dni { get; set; }
        public String Correo { get; set; }
        public String Telefono { get; set; }
        public String Direccion { get; set; }
        public String Perfil { get; set; }
        public String Estado { get; set; }
    }
    public class UsuarioRequestChangePassTemp
    {
        public Int16 Id { get; set; }
        public String ClaveAnterior { get; set; }
        public String ClaveNueva { get; set; }
    }
    public class showEditarUsuario
    {
        public Int16 Id { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String Dni { get; set; }
        public String Correo { get; set; }
        public String Telefono { get; set; }
        public String Direccion { get; set; }
        public Byte PerfilId { get; set; }
    }
}