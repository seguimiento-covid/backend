using System;
using System.Collections.Generic;

namespace CEN
{
    public class MesAnio
    {
        public Int32 Mes { get; set; }
        public Int32 Anio { get; set; }
    }

    public class reportePeriodoResponse
    {
        public String Dni { get; set; }
        public String TrabNombre { get; set; }
        public String Perfil { get; set; }
        public String Dias { get; set; }
    }

    public class reportePeriodoVista
    {
        public String Dni { get; set; }
        public String Nombre { get; set; }
        public String Perfil { get; set; }
        public Int32[] Dias { get; set; }
    }

    public class reporteDiaVista
    {
        public Int16 Id { get; set; }
        public String Dni { get; set; }
        public String Nombre { get; set; }
        public String Perfil { get; set; }
        public Int16 Estado { get; set; }
        public String Descripcion { get; set; }
        public String AppId { get; set; }
    }
    public class reporteDiaPersona
    {
        public Int16 Id { get; set; }
        public String Dni { get; set; }
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String Telefono { get; set; }
        public String Perfil { get; set; }
        public Int32 IdReporte { get; set; }
        public String[] Sintomas { get; set; }
        public DateTime FechaReporte { get; set; }
        public String Correo { get; set; }
        public String Direccion { get; set; }
        public Int16 CantDosis { get; set; }
        public Boolean Confirmado { get; set; }
        public Boolean Sospechozo { get; set; }
        public Boolean Positivo { get; set; }

        public Decimal Temperatura { get; set; }
        public String Comentario { get; set; }
        public String[] Signos { get; set; }
    }
    public class FormCovidCEN
    {
        public Int32 Id { get; set; }
        public String Dni { get; set; }
        public String Nombre { get; set; }
        public String Perfil { get; set; }
        public Boolean HasCovid { get; set; }
    }

    public class RecibeFormCovidCEN
    {
        public Int32 Id { get; set; }
        public Boolean HasCovid { get; set; }
        public Int32 UserMaster { get; set; }
    }
    public class EnvioRangoCEN
    {
        public String FechaInicio { get; set; }
        public String FechaFinal { get; set; }
        public int UserId { get; set; }
    }

    public class reporteRangoVista
    {
        public String Dni { get; set; }
        public String Nombre { get; set; }
        public String Perfil { get; set; }
        public Int32[] Dias { get; set; }
    }

    public class ReporteExportExcel
    {
        public Int32 Id { get; set; }
        public String Dni { get; set; }
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String Telefono { get; set; }
        public String Perfil { get; set; }
        public Int32 IdReporte { get; set; }
        public String[] Sintomas { get; set; }
        public DateTime FechaReporte { get; set; }
        public String Correo { get; set; }
        public String Direccion { get; set; }
        public Int16 CantDosis { get; set; }
        public Boolean Confirmado { get; set; }
        public Boolean Sospechozo { get; set; }
        public Boolean Positivo { get; set; }
        public Boolean Normal { get; set; }
        public Decimal Temperatura { get; set; }
        public String Comentario { get; set; }
        public String[] Signos { get; set; }
    }



}