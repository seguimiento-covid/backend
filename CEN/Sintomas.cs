using System;

namespace CEN
{
    public class Sintomas
    {
        public Int32 SintomaId { get; set; }
        public String Detalle { get; set; }
    }

    public class SintomaRecive
    {
        public Int32 SintomaId { get; set; }
        public String OtroDetalle { get; set; }
    }

    public class SintomaPersonalReporte
    {
        public String Detalle { get; set; }
        public String OtroDetalle { get; set; }
    }
}