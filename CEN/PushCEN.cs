using System;
using System.Collections.Generic;

namespace CEN
{
    public class PushRecibidoFrontCEN
    {
        public String Cabecera { get; set; }
        public String Texto { get; set; }
        public List<String> Personas { get; set; }
    }
    public class PushSendCEN
    {
        public String app_id { get; set; }
        public Content contents { get; set; }
        public Headings headings { get; set; }
        public List<String> include_player_ids { get; set; }
    }

    public class PushResponse
    {
        public String Id { get; set; }
        public Int32 Recipients { get; set; }
        public Object External_Id { get; set; }
        public String[] Errors { get; set; }
    }
    public class Content
    {
        public String en { get; set; }
    }
    public class Headings
    {
        public String en { get; set; }
    }
}